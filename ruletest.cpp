#include "ruletest.h"
#include "ui_ruletest.h"
#include "global/globalrule.h"
#include "global/substringexp.h"

#include <QMessageBox>
#include <QNetworkAccessManager>
#include <QNetworkReply>

RuleTest::RuleTest(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RuleTest)
{
    ui->setupUi(this);

    // 标题
    this->setWindowTitle("规则测试: " + GlobalRule::subMark);
}

RuleTest::~RuleTest()
{
    delete ui;
}

/**
* 获得网页的html源码
*/
void RuleTest::getHtml(QString url)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager();

    QUrl webUrl(url);
    QNetworkRequest request(webUrl);

    QString method = ui->comboBox_2->currentText();

    qDebug() << method;

    if(method == "GET")
    {
        //向网页发起get请求
        manager->get(request);
    }

    if(method == "POST")
    {
        //向网页发起post请求
        QByteArray postData ="";
        manager->post(request, postData);
    }

    // 获得html源码
    QObject::connect(manager, &QNetworkAccessManager::finished, [this](QNetworkReply *reply) {
        if (reply->error()) {
            QString error = reply->errorString();
            //qDebug() << "Error: " << reply->errorString();
            ui->plainTextEdit->setPlainText(error);
        } else {
            QString html = reply->readAll();

            // qDebug() << "HTML: " << html;

            this->ceshiInfo(html);

            // ui->plainTextEdit->setPlainText(html); // 在编辑框中显示获取到的内容
        }
        reply->deleteLater();
    });
}

/**
* 获得网页的json对象
*/
void RuleTest::getJson(QString url)
{
    qDebug() << "暂时不需要";
}

/**
 * 点击测试采集按钮
 */
void RuleTest::on_pushButton_clicked()
{
    // 清空编辑框中的内容
    ui->plainTextEdit->setPlainText("");

    QString url = ui->comboBox->currentText().trimmed();

    if(url.isEmpty())
    {
        QMessageBox::information(this, "提示", "请填写测试地址");
        ui->comboBox->setFocus();
        return;
    }

    this->getHtml(url);
}

/**
* 在测试编辑框显示输出内容（根据规则）
*/
void RuleTest::ceshiInfo(QString url)
{
    QString subMark = GlobalRule::subMark;

    if(subMark == "html")
    {
        this->ceshiHtml(url);
    }

    if(subMark == "json")
    {
        this->ceshiJson(url);
    }
}

/**
 * 测试html前后截取的数据
 */
void RuleTest::ceshiHtml(QString url)
{
    QString initText = "请求页面 默认页 " + ui->comboBox->currentText();
    ui->plainTextEdit->insertPlainText(initText);
    ui->plainTextEdit->appendPlainText("");
    ui->plainTextEdit->appendPlainText("");

    QJsonArray htmlSubRule = GlobalRule::htmlSubRule;
    int rowCount = htmlSubRule.count(); // 数组大小

    for (int i = 0; i < rowCount; ++i) {
        // 数组中为对象列表
        QJsonValue  jsonValue = htmlSubRule[i];

        QJsonObject jsonObj = jsonValue.toObject();

        // qDebug() << jsonObj; // jsonObj是对象类型

        QString LabelName = jsonObj.value("LabelName").toString(); // 获得标题标签
        LabelName = "【" + LabelName + "】: ";
        QString StartStr = jsonObj.value("StartStr").toString(); // 获得开始字符串
        QString EndStr = jsonObj.value("EndStr").toString(); // 获得结束字符串

        QString neirong = SubStringExp::strMid(url, StartStr, EndStr); // 在html截取需要的内容

        // 把测试结果显示到编辑框中
        SubStringExp::textStyle(ui->plainTextEdit, QColor("red"), QFont::Bold); // 设置字体开始样式
        ui->plainTextEdit->appendPlainText(LabelName);
        SubStringExp::textStyle(ui->plainTextEdit, QColor("black"), QFont::Normal);
        ui->plainTextEdit->insertPlainText(neirong);
    }
}

/**
 * 测试json格式的数据
 */
void RuleTest::ceshiJson(QString url)
{
    QString initText = "请求页面 默认页 " + ui->comboBox->currentText();
                                                   ui->plainTextEdit->insertPlainText(initText);
    ui->plainTextEdit->appendPlainText("");
    ui->plainTextEdit->appendPlainText("");

    QJsonArray jsonSubRule = GlobalRule::jsonSubRule;
    int rowCount = jsonSubRule.count(); // 数组大小

    for (int i = 0; i < rowCount; ++i) {
        // 数组中为对象列表
        QJsonValue  jsonValue = jsonSubRule[i];

        QJsonObject jsonObj = jsonValue.toObject();

        // qDebug() << jsonObj; // jsonObj是对象类型

        QString LabelName = jsonObj.value("LabelName").toString(); // 获得标题标签
        LabelName = "【" + LabelName + "】: ";
        QString jsonExp = jsonObj.value("jsonExp").toString(); // 获得json表达式

        QString neirong = SubStringExp::jsonMid(url, jsonExp); // 在json对象中获得想要的内容

        // 把测试结果显示到编辑框中
        SubStringExp::textStyle(ui->plainTextEdit, QColor("red"), QFont::Bold); // 设置字体开始样式
        ui->plainTextEdit->appendPlainText(LabelName);
        SubStringExp::textStyle(ui->plainTextEdit, QColor("black"), QFont::Normal);
        ui->plainTextEdit->insertPlainText(neirong);
    }

}
