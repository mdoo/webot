QT       += core gui network axcontainer sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addweburl.cpp \
    getcookie.cpp \
    global/globalrule.cpp \
    global/substringexp.cpp \
    jsonrule.cpp \
    jsonurl.cpp \
    main.cpp \
    mainwindow.cpp \
    ruletest.cpp \
    substring.cpp \
    userlogin.cpp

HEADERS += \
    addweburl.h \
    getcookie.h \
    global/globalrule.h \
    global/substringexp.h \
    jsonrule.h \
    jsonurl.h \
    mainwindow.h \
    ruletest.h \
    substring.h \
    userlogin.h

FORMS += \
    addweburl.ui \
    getcookie.ui \
    jsonrule.ui \
    jsonurl.ui \
    mainwindow.ui \
    ruletest.ui \
    substring.ui \
    userlogin.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc
