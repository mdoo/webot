#ifndef USERLOGIN_H
#define USERLOGIN_H

#include <QWidget>
#include <QSqlDatabase>

namespace Ui {
class UserLogin;
}

class UserLogin : public QWidget
{
    Q_OBJECT

public:
    explicit UserLogin(QWidget *parent = nullptr);
    ~UserLogin();

    bool DbConnect();

    QString hash_encryption(const QString &password);

    QString MD5_encryption(const QString &data);

    bool checkHashedPassword(const QString &password, const QString &hashedPassword);

    void login(QString username, QString password);

private slots:
    void on_pushButton_clicked();

private:
    Ui::UserLogin *ui;
};

#endif // USERLOGIN_H
