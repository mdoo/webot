#include "jsonurl.h"
#include "ui_jsonurl.h"

#include <QNetworkAccessManager>
#include <QMessageBox>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStyleFactory>
#include <typeinfo>

JsonUrl::JsonUrl(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::JsonUrl)
{
    ui->setupUi(this);

    // 初始化treeWidget显示样式
    ui->treeWidget->clear(); // 清空数据
    ui->treeWidget->setHeaderHidden(true); // 是否隐藏表头
    ui->treeWidget->setStyle(QStyleFactory::create("windows")); // 加上虚线

    // 监听item单击事件
    connect(ui->treeWidget, &QTreeWidget::itemClicked, this, &JsonUrl::itemClicked);
}

JsonUrl::~JsonUrl()
{
    delete ui;
}

/**
 * @brief JsonUrl::on_pushButton_clicked
 * 获取JSON数据，并且用树状形式列出
 */
void JsonUrl::on_pushButton_clicked()
{
    if(ui->lineEdit->text().trimmed().isEmpty())
    {
        QMessageBox::information(this, "提示", "json地址不能为空");
        ui->lineEdit->setFocus();
        return;
    }
    ui->treeWidget->clear(); // 清空数据

    QString webUrl = ui->lineEdit->text().trimmed();

    getJson(webUrl);  // 获得json网址的数据
}

/**
 * 获得QJsonDocument对象
 * webUrl: json网址
 */
void JsonUrl::getJson(QString webUrl)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager;
    QUrl url(webUrl);
    QNetworkRequest request(url);

    // 对数据进行解析
    QObject::connect(manager, &QNetworkAccessManager::finished, [this](QNetworkReply *reply) {
        if(reply->error()) {
            QMessageBox::information(this, "提示", reply->errorString());
        } else {
            // 将字符串转换为QJsonDocument
            QJsonDocument jsonDoc = QJsonDocument::fromJson(reply->readAll()); // QJsonDocument格式

            // 检查jsonDoc是否包含一个有效的JSON对象
            if (jsonDoc.isNull())
            {
                QJsonParseError err;
                qDebug() << err.errorString();
                QMessageBox::information(NULL, "提示", "不是有效的json对象");
                return -1;
            }

            qDebug() << jsonDoc << "\n"; // 输出 QJsonDocument

            this->checkObjOrArr(jsonDoc); // 判断QJsonDocument是对象还是数组

            // int nLen = rootObj.length(); // 对象长度
            // qDebug() << " nLen = " << nLen << "\n";qDebug() << " rootObj.keys() = " << rootObj.keys() << "\n"; // 对象的key
            // QList<QString> mykeys = rootObj.keys();
            // qDebug() << mykeys << "\n";
            // QByteArray request_json = jsonDoc.toJson( QJsonDocument::JsonFormat::Indented ); // 将QJsonDocument转换为Json
            // qDebug() << request_json << "\n";
            // return request_json; // 返回json数据
        }
        // 在当前对象的所有事件处理完成后再删除对象
        reply->deleteLater();
        reply = nullptr;
    });

    //向网页发起get请求
    manager->get(request);
}

/**
 * 判断QJsonDocument是 对象 还是 数组
 */
void JsonUrl::checkObjOrArr(QJsonDocument jsonDoc)
{
    QTreeWidgetItem *current_item = new QTreeWidgetItem();
    ui->treeWidget->addTopLevelItem(current_item); // 添加顶层节点
    current_item->setExpanded(true); // 是否展开节点
    current_item->setIcon(0,QIcon(QString(":/images/node.ico")));  // 设置父节点的图片

    /**** QJsonDocument如果是对象，显示json树列表 ****/
    if(jsonDoc.isObject())
    {
        current_item->setText(0, "根节点");

        QJsonObject rootObj = jsonDoc.object(); // 将QJsonDocument转换为QJsonObject
        qDebug() << rootObj << "\n"; // QJsonObject        

        this->AnalysisJsonObj(rootObj, current_item); // 显示节点
    }

    /**** QJsonDocument如果是数组，显示json树列表 ****/
    if(jsonDoc.isArray())
    {
        current_item->setText(0, "[ ] 根节点");

        QJsonArray rootArray = jsonDoc.array(); // 将QJsonDocument转换为QJsonArray
        QJsonValue obj_value = rootArray;

        this->AnalysisJsonValue(obj_value, current_item);  // 显示节点
    }
}

/**
 * 添加节点
 * jsonObj: QJsonObject对象数据
 * root： 节点
 */
void JsonUrl::AnalysisJsonObj(QJsonObject &jsonObj, QTreeWidgetItem *root )
{
    QList<QString> mykeys = jsonObj.keys();  // 对象的全部key列表
    qDebug() << mykeys.length();

    for (int i = 0; i < mykeys.length(); ++i)
    {
        QString keyname = mykeys[i]; // 对应的key的值

        QTreeWidgetItem *current_item = new QTreeWidgetItem();
        if(root)
        {
            root->addChild(current_item);
            root->setIcon(0, QIcon(QString(":/images/node.ico")));  // 设置父节点的图片
        }

        QJsonValue obj_value = jsonObj.value(keyname);
        this->AnalysisJsonValue(obj_value, current_item);
        this->SetItem(current_item, keyname, obj_value);
    }
}

/**
 * 显示节点下的数据（显示key值）
 * 迭代显示数据 (判断是否对象或者数组)
 * value： 键对应的值
 * root： 节点
 */
void JsonUrl::AnalysisJsonValue(QJsonValue &value, QTreeWidgetItem *root)
{
    if (value.isObject()) // 如果是对象
    {
        QJsonObject jsonObj = value.toObject();
        this->AnalysisJsonObj(jsonObj, root);
    }
    else if(value.isArray())  // 如果是数组
    {
        QJsonArray arr = value.toArray();

        for (int i = 0; i < arr.size(); ++ i)
        {
            QTreeWidgetItem * current_item = new QTreeWidgetItem();

            if(root)
            {
                root->addChild(current_item); // 添加数组节点
                root->setIcon(0, QIcon(QString(":/images/node.ico")));  // 设置父节点的图片（数组节点）
            }

            QString keyname = "[" + QString::number(i) + "]"; // key的名字 (数组节点)
            QJsonValue jv = arr[i]; // key的值

            this->SetItem(current_item, keyname, jv);

            AnalysisJsonValue(jv, current_item); // 迭代节点上的数据（对象或数组，显示格式QList）
        }
    }
    else if (value.isString())
    {
    }
    else if (value.isDouble())
    {
    }
}

/**
 * 显示节点 （是对象显示{},是数组显示[]）
 * item: 节点（添加节点声明）
 * keyname: key名
 * value: key对应的值
 */
void JsonUrl::SetItem(QTreeWidgetItem *item, QString &keyname, QJsonValue &value)
{
    // if (value.isObject()) // 如果是 对象
    // {
        // 开头显示“{}”
        // keyname.prepend("{ }");  // {}
    // }
    // else

    if (value.isArray()) // 如果是 数组
    {
        keyname.prepend("[ ] ");
    }
    else if(value.isString()) // 如果是 字符串
    {
        QString value_str = value.toString();
        keyname.append( " : " );
        keyname.append(value_str); // 显示字符串键值对, keyname:value
        item->setIcon(0,QIcon(QString(":/images/item.png")));  // 设置最终节点的图片
    }
    else if(value.isDouble()) // 如果是 数字
    {
        double double_value = value.toDouble();
        keyname.append(" : ");
        keyname.append(QString::number(double_value)); // 显示数字键值对, keyname:value
        item->setIcon(0,QIcon(QString(":/images/item.png")));  // 设置最终节点的图片
    } else if(value.isNull()) // 如果是 空
    {
        QString value_str = "";
        keyname.append(" : ");
        keyname.append(value_str); // 显示数字键值对, keyname:value
        item->setIcon(0,QIcon(QString(":/images/item.png")));  // 设置最终节点的图片
    }

    item->setExpanded(true); // 节点展开
    item->setText(0, keyname); // 节点添加数据
}

/**
 * 发送json规则给父窗口
 */
void JsonUrl::on_pushButton_2_clicked()
{
    QString jsonUrl = ui->lineEdit_2->text().trimmed();

    if(jsonUrl.isEmpty())
    {
        QMessageBox::information(this, "提示", "JSON表达式不能为空");
        return;
    }

    emit sendJsonRule(jsonUrl); // 发送信号

    this->close();
}

//鼠标单击触发
void JsonUrl::itemClicked(QTreeWidgetItem *item, int column)
{
    qDebug() << "鼠标点击" << item->text(column);

    // 判断如果点击的节点有子节点那么json表达式为空
    if(!item->childCount() == 0)
    {
        ui->lineEdit_2->setText("");
        return;
    }

    // 获得keyname
    QString str = item->text(column); // keyname:value 形式的字符串
    QStringList strList = str.split(" : "); // 用 " : " 分割字符串
    QString keyname = strList.at(0);

    // 获得父节点的值
    QTreeWidgetItem *parent_item = item->parent();
    QString parent_keyname = parent_item->text(column);

    ui->lineEdit_2->setText(parent_keyname + keyname); // 拼接json表达式规则
}
