#ifndef RULETEST_H
#define RULETEST_H

#include <QWidget>

namespace Ui {
class RuleTest;
}

class RuleTest : public QWidget
{
    Q_OBJECT

public:
    explicit RuleTest(QWidget *parent = nullptr);
    ~RuleTest();

    void getHtml(QString url);

    void getJson(QString url);

    void ceshiInfo(QString url);

    void ceshiHtml(QString url);

    void ceshiJson(QString url);

private slots:
    void on_pushButton_clicked();

private:
    Ui::RuleTest *ui;
};

#endif // RULETEST_H
