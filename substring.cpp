#include "substring.h"
#include "ui_substring.h"

#include <QMessageBox>

SubString::SubString(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SubString)
{
    ui->setupUi(this);
}

SubString::~SubString()
{
    delete ui;
}

void SubString::on_pushButton_2_clicked()
{
    this->close();
}

/**
 * 添加数据到父窗口
 */
void SubString::on_pushButton_clicked()
{
    QString biaoQian = ui->lineEdit->text().trimmed(); // 标签名
    QString startString = ui->plainTextEdit->toPlainText(); // 开始字符串
    QString endString = ui->plainTextEdit_2->toPlainText(); // 结束字符串

    if(biaoQian.isEmpty())
    {
        QMessageBox::information(this, "提示", "标签名不得为空");
        ui->lineEdit->setFocus(); // 设置光标焦点
        return;
    }

    // 向父窗口提交数据
    emit startEndString(biaoQian, startString, endString);

    this->close();
}

