#include "mainwindow.h"
#include "addweburl.h"
#include "getcookie.h"
#include "jsonrule.h"
#include "ui_mainwindow.h"
#include "substring.h"
#include "ruletest.h"
#include "global/globalrule.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QString>
#include <QDebug>
#include <QMessageBox>
#include <QAxObject>
#include <QFileInfo>
#include <QDir>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // 初始化tableWidget
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置单元格不能编辑
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows); // 设置选择行为，以行为单位
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers); // 禁止编辑

    ui->tableWidget_2->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置单元格不能编辑
    ui->tableWidget_2->setSelectionBehavior(QAbstractItemView::SelectRows); // 设置选择行为，以行为单位
    ui->tableWidget_2->setEditTriggers(QAbstractItemView::NoEditTriggers); // 禁止编辑

    ui->tableWidget_3->setEditTriggers(QAbstractItemView::NoEditTriggers);  // 设置单元格不能编辑
    ui->tableWidget_3->setSelectionBehavior(QAbstractItemView::SelectRows); // 设置选择行为，以行为单位
    // ui->tableWidget_3->setSelectionMode(QAbstractItemView::SingleSelection); // 设置选择模式，选择单行
    ui->tableWidget_3->setEditTriggers(QAbstractItemView::NoEditTriggers); // 禁止编辑
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * 开始采集数据
 */
void MainWindow::on_pushButton_5_clicked()
{
    // 要采集的网址
    int urlCount = ui->tableWidget_3->rowCount();

    if(urlCount == 0)
    {
        ui->tabWidget->setCurrentIndex(0); // 跳转指定索引的tab选项卡
        QMessageBox::information(this, "提示", "采集网址不能为空");
        return;
    }

    // 文件目录位置
    QString path = ui->lineEdit_4->text().trimmed();

    // 文件名字
    QString fileName = ui->lineEdit_5->text().trimmed();

    if(path.isEmpty())
    {
        ui->tabWidget->setCurrentIndex(2); // 跳转指定索引的tab选项卡
        ui->stackedWidget_2->setCurrentIndex(1);

        QMessageBox::information(this, "提示", "保存路径位置不能为空");
        ui->lineEdit_4->setFocus();

        return;
    }

    if(fileName.isEmpty())
    {
        ui->tabWidget->setCurrentIndex(2); // 跳转指定索引的tab选项卡

        QMessageBox::information(this, "提示", "文件名不能为空");
        ui->lineEdit_5->setFocus();

        return;
    }

    // 获得excel文件的路径
    QString pathFile = path + fileName;

    // 创建excel文件名
    this->createExcel(pathFile);


//    QNetworkAccessManager *manager = new QNetworkAccessManager;
//    QUrl url(webUrl);
//    QNetworkRequest request(url);

//    qDebug() << "dfd";

//    // 对数据进行解析
//    QObject::connect(manager, &QNetworkAccessManager::finished, [pathFile, this](QNetworkReply *reply) {
//        if(reply->error()) {
//            QMessageBox::information(this, "提示", reply->errorString());
//            qDebug() << reply->errorString();
//        } else {
//            // 将字符串转换为QJsonDocument
//            QJsonDocument jsonDoc = QJsonDocument::fromJson(reply->readAll());

//            // 确保JSON文档是一个对象
//            // if (!jsonDoc.isObject()) {
//            //     qDebug() << "JSON文档不是一个对象";
//            //     return -1;
//            // }

//            // 获取对象
////            if(jsonDoc.isObject()) {
////                // 获取JSON对象
////                QJsonObject jsonObj = jsonDoc.object();
////                qDebug() << jsonObj;

////                // 获取并输出字段
////                QJsonValue nameValue = jsonObj.value("pic_url");
////                qDebug() << nameValue.toString();

////                // 如果你想输出所有字段，可以使用以下代码：
////                // 获取所有键（字段名）
////                QStringList keys = jsonObj.keys();
////                foreach (const QString &key, keys) {
////                    QJsonValue value = jsonObj.value(key);
////                    qDebug() << key << ":" << value;
////                }

////            }

//            // 确保它是一个数组
//            if (!jsonDoc.isArray()) {
//                qDebug() << "不是一个JSON数组";
//                return -1;
//            }

//            // 获取数组
//            if(jsonDoc.isArray()) {
//                // 获取JSON数组
//                QJsonArray jsonArray = jsonDoc.array();
//                qDebug() << jsonArray; // 输出整个json数组

//                // 遍历数组并输出每个对象的字段
//                QStringList keyValueList;
//                for (const QJsonValue &value : jsonArray) {
//                    // 确保每个元素都是一个对象
//                    if (!value.isObject()) {
//                        qDebug() << "不是一个JSON对象";
//                        continue;
//                    }

//                    // json数组 转成 json对象
//                    QJsonObject jsonObj = value.toObject(); // 一条数据

//                    // 检查对象中是否有相关字段
//                    if (jsonObj.contains("add_time")) {
//                        // 获取键的值
//                        QJsonValue jsonValue = jsonObj.value("desc");

//                        // 输出字段的值
//                        qDebug() << jsonValue; // 输出内容: QJsonValue(string, "info")

//                        QString keyValue = jsonValue.toVariant().toString();

//                        qDebug() << keyValue; // 输出内容: info

//                        keyValueList.append(keyValue);

//                        // 写入数据到Excel文件中
////                        writeExcel(pathFile, keyValue);
//                    }
//                }

//                qDebug() << keyValueList;
//                return 1;
//            }
//        }
//        // 在当前对象的所有事件处理完成后再删除对象
//        reply->deleteLater();
//        reply = nullptr;
//        return 1;
//    });

//    //向网页发起get请求
//    manager->get(request);
}

/**
 * @brief MainWindow::createExcel
 * @param path
 * @return
 * 创建excel表格
 */
int MainWindow::createExcel(QString path)
{
    // path = "d:/data.xlsx";

    QAxObject *excel = new QAxObject(this);
    excel->setControl("Excel.Application");//连接Excel控件
    excel->dynamicCall("SetVisible (bool Visible)", "false"); //不 显示窗体
    excel->setProperty("DisplayAlerts", false); // 不显示任何警告信息。如果为true那么在关闭是会出现类似“文件已修改，是否保存”的提示

    QAxObject *workbooks = excel->querySubObject("WorkBooks"); //获取工作簿集合
    workbooks->dynamicCall("Add"); // 新建一个工作簿
    QAxObject *workbook = excel->querySubObject("ActiveWorkBook"); //获取当前工作簿
    // QAxObject *worksheets = workbook->querySubObject("Sheets"); //获取工作表集合
    // QAxObject *worksheet = worksheets->querySubObject("Item(int)", 1); //获取工作表集合的工作表1，即sheet1
    workbook->dynamicCall("SaveAs(const QString&)", QDir::toNativeSeparators(path)); // 保存至filepath，注意一定要用QDir::toNativeSeparators将路径中的"/"转换为"\"，不然一定保存不了。
    workbook->dynamicCall("Close()"); // 关闭工作簿
    excel->dynamicCall("Quit()"); // 关闭excel
    delete excel;
    excel = NULL;
    return 0;
}

/**
 * @brief MainWindow::writeExcel
 * @param path
 * @param value
 * @return
 * excel表格单元格写入
 */
int MainWindow::writeExcel(QString path, QString value)
{
    QAxObject *excel = NULL;
    QAxObject *workbooks = NULL;
    QAxObject *workbook = NULL;
    excel = new QAxObject(this);
    excel->setControl("Excel.Application");
    if (!excel) {
        QMessageBox::critical(this, "错误信息", "EXCEL对象丢失");
        return -1;
    }

    excel->dynamicCall("SetVisible(bool)", false);
    workbooks = excel->querySubObject("WorkBooks");
    workbooks->querySubObject("Add");
    workbook = workbooks->querySubObject("Open(const QString&)",path);
    QAxObject * worksheet = workbook->querySubObject("WorkSheets(int)", 1); // 获取第一个工作sheet

//    QAxObject *cellInsert = worksheet->querySubObject("Cells(int,int)", 5, 6);
//    cellInsert->setProperty("Value", "a2aaa");  //设置单元格值
//    delete cellInsert;

    //插入数据
    int cellRow = 2; // 行
    int cellCol = 1; // 列

    QAxObject *cellInsert = worksheet->querySubObject("Cells(int,int)", cellCol, cellRow);
    qDebug() << "+++++++" << value;
    cellInsert->setProperty("Value", value);  //设置单元格值
    delete cellInsert;

    // 关闭excel
    workbook->dynamicCall("Close(Boolean)",true);
    excel->dynamicCall("Quit(void)");
    delete excel;
    excel = NULL;
    return 0;
}

/** excel表格读取 **/
int MainWindow::readExcel(QString path)
{
    QAxObject *excel = NULL;
    QAxObject *workbooks = NULL;
    QAxObject *workbook = NULL;
    excel = new QAxObject(this);
    excel->setControl("Excel.Application");
    if (!excel) {
        QMessageBox::critical(this, "错误信息", "EXCEL对象丢失");
        return -1;
    }

    excel->dynamicCall("SetVisible(bool)", false);
    workbooks = excel->querySubObject("WorkBooks");
    workbooks->querySubObject("Add");
    workbook = workbooks->querySubObject("Open(const QString&)",path);
    QAxObject * worksheet = workbook->querySubObject("WorkSheets(int)", 1); // 获取第一个工作sheet

    QAxObject * usedrange = worksheet->querySubObject("UsedRange");//获取该sheet的使用范围对象
    QAxObject * rows = usedrange->querySubObject("Rows");
    QAxObject * columns = usedrange->querySubObject("Columns");
    /*获取行数和列数*/

    int intCols = columns->property("Count").toInt();
    int intRows = rows->property("Count").toInt();
    int intRowStart = usedrange->property("Row").toInt();
    int intColStart = usedrange->property("Column").toInt();
    /*获取excel内容*/
    for (int i = intRowStart; i < intRowStart + intRows; i++)  //行
    {
        for (int j = intColStart; j < intColStart + intCols; j++)
        {
            QAxObject *cell = worksheet->querySubObject("Cells(int,int)", i, j);
            QString value = cell->dynamicCall("Value2()").toString();
            qDebug() << __FILE__ << __LINE__ << value;
            delete cell;
        }
    }

    // 关闭excel
    workbook->dynamicCall("Close(Boolean)",true);
    excel->dynamicCall("Quit(void)");
    delete excel;
    excel = NULL;
    return 0;
}

/**
 * @brief MainWindow::openDir
 * 选择文件目录
 */
void MainWindow::openDir()
{
    QString path = QFileDialog::getExistingDirectory(this,                 // 父窗口
                                                     tr("Open Directory"), // 对话框标题
                                                     "",               // 打开的默认路径
                                                     QFileDialog::ShowDirsOnly // 只在文件对话框中显示目录。默认情况下同时显示文件和目录。
                                                         | QFileDialog::DontResolveSymlinks // 不要解析文件对话框中的符号链接。默认情况下符号链接是解析的
                                                     );


    if(path.isEmpty())
    {
        QMessageBox::warning(this, "提示", "文件夹路径为空");
        return;
    }

    bool end =  path.endsWith("/");  // 如果目录最后是以为“/”结尾

    if(!end)
    {
        path = path + "/";
    }

    ui->lineEdit_4->setText(path);

//    qDebug()<<"---------"<<path;//d:/Desktop/image
//    QDir dir(path);
//    //返回绝对路径，包含.或者..
//    qDebug()<< dir.absoluteFilePath(path);//d:/Desktop/image
//    //返回绝对路径，不包含.或者..
//    qDebug()<< dir.absolutePath();//d:/Desktop/image
//    //返回文件夹名称
//    qDebug()<< dir.dirName();//image
//    //文件夹是否存在
//    qDebug()<< dir.exists();//true
//    //返回文件夹路径名，如果文件夹路径是相对的，返回的路径也是相对的，包含.或者..
//    qDebug()<< dir.filePath(path);//d:/Desktop/image
//    qDebug()<< dir.isAbsolute();//true
//    qDebug()<< dir.path();//d:/Desktop/image
//    qDebug()<< dir.mkdir(path);//false
//    qDebug()<< dir.rmdir(path);//false
//    qDebug()<< dir.currentPath();//D:/Cam/build-FileDialogDemo-Desktop_Qt_5_9_1_MinGW_32bit-Debug
}

/**
 * @brief MainWindow::on_pushButton_6_clicked
 * 弹出选择文件夹目录对话框
 */
void MainWindow::on_pushButton_6_clicked()
{
    this->openDir();
}

/**
 * @brief MainWindow::on_pushButton_clicked
 * 打开添加json规则窗口
 */
void MainWindow::on_pushButton_clicked()
{
    JsonRule *jsonRule = new JsonRule();
    jsonRule->show();

    // 开启信号监听,监听添加接收到的json规则
    connect(jsonRule, &JsonRule::addJsonRule, this, &MainWindow::receive_JsonRule);
}

/**
 * 接收子窗口传送过来的数据，接收json规则，添加到json规则中
 */
void MainWindow::receive_JsonRule(QString biaoQian, QString jsonRule)
{
    // 插入数据
    int curRow = ui->tableWidget->rowCount();

    ui->tableWidget->insertRow(curRow);

    QTableWidgetItem *s1 = new QTableWidgetItem;
    QTableWidgetItem *s2 = new QTableWidgetItem;

    s1->setText(biaoQian);
    s2->setText(jsonRule);

    ui->tableWidget->setItem(curRow, 0, s1);
    ui->tableWidget->setItem(curRow, 1, s2);

    // 组装json采集规则的全局参数
    this->jsonSubRule(curRow);
    qDebug() << GlobalRule::jsonSubRule;
}

/**
 * 接收子窗口传送过来的数据，接收字符串截取规则
 */
void MainWindow::receive_startEndString(QString biaoQian, QString startString, QString endString)
{
    // 插入数据
    int curRow = ui->tableWidget_2->rowCount();

    ui->tableWidget_2->insertRow(curRow);

    QTableWidgetItem *s1 = new QTableWidgetItem;
    QTableWidgetItem *s2 = new QTableWidgetItem;
    QTableWidgetItem *s3 = new QTableWidgetItem;

    s1->setText(biaoQian);
    s2->setText(startString);
    s3->setText(endString);

    ui->tableWidget_2->setItem(curRow, 0, s1);
    ui->tableWidget_2->setItem(curRow, 1, s2);
    ui->tableWidget_2->setItem(curRow, 2, s3);

    // 组装html规则全局参数
    this->htmlSubRule(curRow);
    qDebug() << GlobalRule::htmlSubRule;
}

void MainWindow::on_pushButton_4_clicked()
{
    this->close();
}


void MainWindow::on_radioButton_clicked(bool checked)
{
    if(checked)
    {
        ui->stackedWidget->setCurrentIndex(0);
    }
}


void MainWindow::on_radioButton_2_clicked(bool checked)
{
    if(checked)
    {
        ui->stackedWidget->setCurrentIndex(1);
    }
}

/**
 * 打开前后截取字符串窗口
 */
void MainWindow::on_pushButton_8_clicked()
{
    SubString *subString = new SubString();
    subString->show();

    // 开启信号监听,监听添加接收到的截取字符串规则
    connect(subString, &SubString::startEndString, this, &MainWindow::receive_startEndString);
}

/**
 * 打开添加采集网址窗口
 */
void MainWindow::on_pushButton_10_clicked()
{
    AddWebUrl *addWebUrl = new AddWebUrl(this);
    addWebUrl->show();

    // 开启信号监听，监听添加接受到的采集网址
    connect(addWebUrl, &AddWebUrl::LineWebUrl, this, &MainWindow::receive_LineWebUrl);
}

/**
 * 接收子窗口传送过来的添加的采集网址
 */
void MainWindow::receive_LineWebUrl(QStringList webUrls)
{
    int lineNum = webUrls.count(); // QStringlist 数量
    qDebug() << webUrls;  

    // 添加所有采集网址
    for (int i = 0; i < lineNum; ++i) {
        int curRow = ui->tableWidget_3->rowCount(); // tableWidget 行数
        ui->tableWidget_3->insertRow(curRow);

        QTableWidgetItem *url = new QTableWidgetItem;
        url->setText(webUrls.at(i));
        ui->tableWidget_3->setItem(curRow, 0, url);
    }
}


/**
 * @brief MainWindow::on_pushButton_12_clicked
 * 删除选中的行
 */
void MainWindow::on_pushButton_12_clicked()
{
    QModelIndex index =  ui->tableWidget_3->currentIndex(); // 当前行的索引
    if(index.row() == -1)
    {
        return; // 提示没有选中的内容
    }
    ui->tableWidget_3->removeRow(index.row()); // 删除选中的行
}

/**
 * @brief MainWindow::on_tableWidget_3_clicked
 * @param index
 * 鼠标单击 一行
 */
void MainWindow::on_tableWidget_3_clicked(const QModelIndex &index)
{
    QString webUrls = ui->tableWidget_3->item(index.row(), 0)->text(); // 取出当前行第1列内容

    qDebug() << webUrls;

}

/**
 * @brief MainWindow::on_pushButton_11_clicked
 * 修改
 */
void MainWindow::on_pushButton_11_clicked()
{
    qDebug() << "修改";
    QModelIndex index =  ui->tableWidget_3->currentIndex(); // 当前行的索引
    qDebug() << index.row();

    QList<QTableWidgetItem *> listItem = ui->tableWidget_3->selectedItems();

    if(listItem.count()==0)
    {
        return;
    }

    foreach (QTableWidgetItem * item, listItem) {
        item->setText("modify");
    }
}

/**
 * @brief MainWindow::on_pushButton_13_clicked
 * 清空所有的数据
 */
void MainWindow::on_pushButton_13_clicked()
{
    int rowCount = ui->tableWidget_3->rowCount(); // 总行数
    qDebug() << rowCount;

    for (int i = 0; i < rowCount; ++i)
    {
        // //每次都清空第0行，清空rowCount次
        ui->tableWidget_3->removeRow(0);
    }
}

/**
 * @brief MainWindow::on_pushButton_14_clicked
 * 打开获取网站cookie窗口
 */
void MainWindow::on_pushButton_14_clicked()
{
    GetCookie *cookie = new GetCookie();
    cookie->show();
}

/**
 * 打开规则测试界面
 */
void MainWindow::on_pushButton_15_clicked()
{
    if(ui->pushButton_17->isChecked())
    {
        GlobalRule::subMark = "json"; // 设置json标志

        if(ui->tableWidget->rowCount() == 0)
        {
            QMessageBox::information(this, "提示", "JSON采集规则不能为空");
            return;
        }
    }

    if(ui->pushButton_16->isChecked())
    {
        GlobalRule::subMark = "html"; // 设置html标志

        if(ui->tableWidget_2->rowCount() == 0)
        {
            QMessageBox::information(this, "提示", "HTML采集规则不能为空");
            return;
        }
    }

    // 显示测试窗口
    RuleTest *ruleTest = new RuleTest();
    ruleTest->show();
}

/**
 * 前后截取html的规则
 */
void MainWindow::htmlSubRule(int curCount)
{
    GlobalRule::Setting("LabelName", ui->tableWidget_2->item(curCount, 0)->text()); // 标签名
    GlobalRule::Setting("StartStr", ui->tableWidget_2->item(curCount, 1)->text()); // 开始字符串
    GlobalRule::Setting("EndStr", ui->tableWidget_2->item(curCount, 2)->text()); // 结束字符串

    GlobalRule::htmlSubObj = GlobalRule::subObj;
    GlobalRule::htmlSubRule.append(GlobalRule::htmlSubObj); // 添加到数组中
}

/**
 * 截取json格式的规则
 */
void MainWindow::jsonSubRule(int curCount)
{
    GlobalRule::Setting("LabelName", ui->tableWidget->item(curCount, 0)->text()); // 标签名
    GlobalRule::Setting("jsonExp", ui->tableWidget->item(curCount, 1)->text()); // json表达式

    GlobalRule::jsonSubObj = GlobalRule::subObj;

    GlobalRule::jsonSubRule.append(GlobalRule::jsonSubObj); // 添加到数组中
}

void MainWindow::on_pushButton_16_clicked(bool checked)
{
    if(checked)
    {
        ui->stackedWidget->setCurrentIndex(0);
    }
}

void MainWindow::on_pushButton_17_clicked(bool checked)
{
    if(checked)
    {
        ui->stackedWidget->setCurrentIndex(1);
    }
}


void MainWindow::on_toolButton_clicked(bool checked)
{
    if(checked)
    {
        ui->stackedWidget_2->setCurrentIndex(0);
    }
}


void MainWindow::on_toolButton_2_clicked(bool checked)
{
    if(checked)
    {
        ui->stackedWidget_2->setCurrentIndex(1);
    }
}

/**
 * 删除选中的行,html规则
 */
void MainWindow::on_pushButton_9_clicked()
{
    QModelIndex index =  ui->tableWidget_2->currentIndex(); // 当前行的索引
    if(index.row() == -1)
    {
        return; // 提示没有选中的内容
    }
    ui->tableWidget_2->removeRow(index.row()); // 删除选中的行

    // html截取规则删除掉一行
    GlobalRule::htmlSubRule.removeAt(index.row());
}

/**
 * 删除选中的行,json规则
 */
void MainWindow::on_pushButton_3_clicked()
{
    QModelIndex index =  ui->tableWidget->currentIndex(); // 当前行的索引
    if(index.row() == -1)
    {
        return; // 提示没有选中的内容
    }
    ui->tableWidget->removeRow(index.row()); // 删除选中的行

    // html截取规则删除掉一行
    GlobalRule::jsonSubRule.removeAt(index.row());
}

