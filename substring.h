#ifndef SUBSTRING_H
#define SUBSTRING_H

#include <QDialog>

namespace Ui {
class SubString;
}

class SubString : public QDialog
{
    Q_OBJECT

public:
    explicit SubString(QWidget *parent = nullptr);
    ~SubString();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

signals:
    void startEndString(QString biaoQian, QString startString, QString endString);

private:
    Ui::SubString *ui;
};

#endif // SUBSTRING_H
