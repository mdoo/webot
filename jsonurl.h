#ifndef JSONURL_H
#define JSONURL_H

#include <QDialog>
#include <QTreeWidgetItem>

namespace Ui {
class JsonUrl;
}

class JsonUrl : public QDialog
{
    Q_OBJECT

public:
    explicit JsonUrl(QWidget *parent = nullptr);
    ~JsonUrl();

    void getJson(QString webUrl);

    void displayJsonTree(QJsonObject &rootObj);

    void AnalysisJsonObj(QJsonObject &jsonObj, QTreeWidgetItem *root);

    void SetItem(QTreeWidgetItem *item, QString &keyname, QJsonValue &value);

    void AnalysisJsonValue(QJsonValue & value, QTreeWidgetItem *root);

    void checkObjOrArr(QJsonDocument jsonDoc);

    //QTreeWidget item,鼠标单击触发
    void itemClicked(QTreeWidgetItem *item, int column);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

signals:
    void sendJsonRule(QString jsonUrl);

private:
    Ui::JsonUrl *ui;
};

#endif // JSONURL_H
