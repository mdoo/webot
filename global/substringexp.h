#ifndef SUBSTRINGEXP_H
#define SUBSTRINGEXP_H

#include <QString>
#include <QPlainTextEdit>

class SubStringExp
{
public:
    SubStringExp();

    static QString strMid(QString srcHtml, QString startStr, QString endStr);

    static QString jsonMid(QString srcHtml, QString jsonExp);

    static void textStyle(QPlainTextEdit *plainTextEdit, QColor fontColor, int fontSize);
};

#endif // SUBSTRINGEXP_H
