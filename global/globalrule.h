#ifndef GLOBALRULE_H
#define GLOBALRULE_H

#include <QString>
#include <QJsonObject>
#include <QJsonArray>

class GlobalRule
{
public:
    GlobalRule();

    static QString subMark;

    static QJsonObject subObj;

    static QJsonObject htmlSubObj;
    static QJsonArray htmlSubRule;

    static QJsonObject jsonSubObj;
    static QJsonArray jsonSubRule;

    static void LoadSetting(QString key, QString defaultValue);

    static void Setting(QString key, QString defaultValue);
};

#endif // GLOBALRULE_H
