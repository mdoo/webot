#include "substringexp.h"

#include <QTextCharFormat>
#include <QPlainTextEdit>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

SubStringExp::SubStringExp()
{

}

//截取两字符串中间的字符串
QString SubStringExp::strMid(QString srcHtml, QString startStr, QString endStr)
{
    int startIndex = srcHtml.indexOf(startStr, 0); // 从索引0开始搜索
    int endIndex = srcHtml.indexOf(endStr, startIndex);

    // 开始字符串的长度
    int startCount = startStr.size();

//    QString subStr = srcHtml.mid(startIndex, endIndex - startIndex);
    QString subStr = srcHtml.mid(startIndex + startCount, endIndex - startIndex - startCount);
    return subStr;
}

// 在json中获得需要的数据
QString SubStringExp::jsonMid(QString srcHtml, QString jsonExp)
{
    qDebug() << srcHtml;

    QJsonDocument jsonDoc = QJsonDocument::fromJson(srcHtml.toUtf8());

    QJsonObject jsonObj = jsonDoc.object();

    qDebug() << jsonObj;

    QJsonValue value = jsonObj.value(jsonExp);

    qDebug() << value;

    return value.toVariant().toString();
}

void SubStringExp::textStyle(QPlainTextEdit* plainTextEdit, QColor fontColor, int fontSize)
{
    QTextCharFormat fontStyle;
    fontStyle.setForeground(fontColor); // 设置字体颜色
    fontStyle.setFontWeight(fontSize); // 设置为粗体
    plainTextEdit->setCurrentCharFormat(fontStyle);
}
