#include "globalrule.h"

QString GlobalRule::subMark;

QJsonObject GlobalRule::subObj;

QJsonObject GlobalRule::htmlSubObj;
QJsonArray GlobalRule::htmlSubRule;

QJsonObject GlobalRule::jsonSubObj;
QJsonArray GlobalRule::jsonSubRule;

GlobalRule::GlobalRule()
{

}

/**
 * 加载json全局数据
 */
void GlobalRule::LoadSetting(QString key, QString defaultValue)
{
    // 从 JSON 对象中获取键值对数据
    defaultValue = subObj[key].toString();
}

/**
 * 设置json全局数据
 */
void GlobalRule::Setting(QString key, QString defaultValue)
{
    // 添加键值对数据到 JSON 对象
    subObj[key] = defaultValue;
}
