#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int createExcel(QString excelPath);

    int writeExcel(QString path, QString value);

    int readExcel(QString path);

    void openDir();

    void htmlSubRule(int curCount);

    void jsonSubRule(int curCount);

private slots:
    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_clicked();

    void receive_JsonRule(QString biaoQian, QString jsonRule); // 接收添加的json规则

    void receive_startEndString(QString biaoQian, QString startString, QString endString); // 接收截取字符串的规则

    void receive_LineWebUrl(QStringList webUrls); // 接收添加的采集网址

    void on_pushButton_4_clicked();

    void on_radioButton_clicked(bool checked);

    void on_radioButton_2_clicked(bool checked);

    void on_pushButton_8_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_12_clicked();

    void on_tableWidget_3_clicked(const QModelIndex &index);

    void on_pushButton_11_clicked();

    void on_pushButton_13_clicked();

    void on_pushButton_14_clicked();

    void on_pushButton_15_clicked();

    void on_pushButton_16_clicked(bool checked);

    void on_pushButton_17_clicked(bool checked);

    void on_toolButton_clicked(bool checked);

    void on_toolButton_2_clicked(bool checked);

    void on_pushButton_9_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
