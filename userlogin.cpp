#include "userlogin.h"
#include "ui_userlogin.h"
#include "mainwindow.h"

#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QCryptographicHash>


UserLogin::UserLogin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserLogin)
{
    ui->setupUi(this);

    setWindowFlags(Qt::MSWindowsFixedSizeDialogHint); // 禁止最大化按钮
    setFixedSize(this->width(),this->height());     // 禁止拖动窗口大小
}

UserLogin::~UserLogin()
{
    delete ui;
}

/**
 * @brief UserLogin::on_pushButton_clicked
 * 用户登录
 */
void UserLogin::on_pushButton_clicked()
{
    QString username = ui->lineEdit->text().trimmed();
    QString password = ui->lineEdit_2->text().trimmed();

    if(username.isEmpty())
    {
        QMessageBox::information(this, "提示", "用户名不能为空");
        ui->lineEdit->setFocus();
        return;
    }

    if(password.isEmpty())
    {
        QMessageBox::information(this, "提示", "密码不能为空");
        ui->lineEdit_2->setFocus();
        return;
    }

    if(this->DbConnect())
    {
        this->login(username, password);
    }
}

/**
 * @brief UserLogin::DbConnect
 * 连接数据库
 */
bool UserLogin::DbConnect()
{
    QStringList dirvers = QSqlDatabase::drivers(); // 获取到qt中所支持的数据库驱动类型

    foreach(QString driver,dirvers){ // 遍历打印出支持的数据库类型
        qDebug() << driver;
    }

    // 连接mysql数据库
    //QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    QSqlDatabase db = QSqlDatabase::addDatabase("QODBC");//连接数据库的类型使用QODBC，因为是通过ODBC进行连接的

    db.setHostName("139.224.247.44"); // 连接服务器
    db.setPort(3306);
    db.setDatabaseName("www.mdoo.cn");
    db.setUserName("www.mdoo.cn");
    db.setPassword("HfeBtM8WspWrwPGM");

    bool ok = db.open(); // 打开数据库

    if(ok){
        // QMessageBox::information(this, "提示", "连接数据库成功");
        return true;
    }
    else {
        QMessageBox::information(this, "提示", "连接数据库失败:" + db.lastError().text());
        return false;
    }
}

/**
 * @brief UserLogin::getPasswordHash
 * hash 加密
 */
QString UserLogin::hash_encryption(const QString &password) {
    QCryptographicHash hash(QCryptographicHash::Sha256);
    hash.addData(password.toLocal8Bit());
    return hash.result().toHex();
}

// MD5 加密
QString UserLogin::MD5_encryption(const QString &data)
{
    QString md5;
    QByteArray bb; // 相当于是QChar的一个vector<>
    bb = QCryptographicHash::hash(data.toUtf8(), QCryptographicHash::Md5);
    md5.append(bb.toHex());
    return md5;
}

/**
 * @brief checkHashedPassword
 * 检测hash密码是否正确
 */
bool UserLogin::checkHashedPassword(const QString &password, const QString &hashedPassword)
{
    QCryptographicHash crypHash(QCryptographicHash::Sha256);
    crypHash.addData(password.toLocal8Bit());
    QByteArray hash = crypHash.result();
    QString generatedHash = hash.toHex();

    qDebug() << generatedHash;

    return hashedPassword == generatedHash;
}

void UserLogin::login(QString username, QString password)
{
    QSqlQuery query;
    query.prepare("select * from mo_users where username = :username");
    query.bindValue(":username", username);   // 使用 ODBC 方法时，可以将编号用实际的占位符代替

    // 执行查询
    if (!query.exec()) {
        qDebug() << "查询失败：" << query.lastError().text();
        return;
    }

    // 遍历查询结果
    if(query.next())
    {
        // qDebug()<<"查询结果不为空!";
        QString hashedPassword = query.value(3).toString();

        if(this->checkHashedPassword(password, hashedPassword))
        {
            // 如果密码正确，登录成功
            MainWindow *w = new MainWindow();
            w->show(); // 显示应用界面

            this->close(); // 关闭登录窗口
        } else {
            // 如果密码不正确
            QMessageBox::information(this, "提示", "你输入的密码不正确");
        }

    }
    else
    {
        // qDebug()<<"查询结果为空!";
        QMessageBox::information(this, "提示", "你输入的用户名不正确");
    }
}
