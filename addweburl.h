#ifndef ADDWEBURL_H
#define ADDWEBURL_H

#include <QDialog>

namespace Ui {
class AddWebUrl;
}

class AddWebUrl : public QDialog
{
    Q_OBJECT

public:
    explicit AddWebUrl(QWidget *parent = nullptr);
    ~AddWebUrl();

    bool checkUrls(const QString &url);

private slots:
    void on_pushButton_clicked();

signals:
    void LineWebUrl(QStringList lineStr); // 提交每行的网址

private:
    Ui::AddWebUrl *ui;
};

#endif // ADDWEBURL_H
