#ifndef GETCOOKIE_H
#define GETCOOKIE_H

#include <QMainWindow>

namespace Ui {
class GetCookie;
}

class GetCookie : public QMainWindow
{
    Q_OBJECT

public:
    explicit GetCookie(QWidget *parent = nullptr);
    ~GetCookie();

private:
    Ui::GetCookie *ui;
};

#endif // GETCOOKIE_H
