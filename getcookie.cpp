#include "getcookie.h"
#include "ui_getcookie.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

GetCookie::GetCookie(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GetCookie)
{
    ui->setupUi(this);

    QNetworkAccessManager manager;
    QUrl url("http://www.example.com");
    QNetworkRequest request(url);

    QNetworkReply *reply = manager.get(request);

    QObject::connect(reply, &QNetworkReply::finished, [&]() {
        if (reply->error()) {
            qDebug() << "Error: " << reply->errorString().toStdString();
        } else {
            // 打印网页内容
            qDebug() << reply->readAll().toStdString();
        }
        reply->deleteLater();
    });
}

GetCookie::~GetCookie()
{
    delete ui;
}
