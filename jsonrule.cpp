#include "jsonrule.h"
#include "jsonurl.h"
#include "ui_jsonrule.h"

#include <QMessageBox>

JsonRule::JsonRule(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::JsonRule)
{
    ui->setupUi(this);
}

JsonRule::~JsonRule()
{
    delete ui;
}


/**
 * @brief MainWindow::on_pushButton_clicked
 * 打开JSON匹配测试窗口
 */
void JsonRule::on_pushButton_clicked()
{
    JsonUrl *jsonUrl = new JsonUrl();
    jsonUrl->show();

    // 开启信号监听
    connect(jsonUrl, &JsonUrl::sendJsonRule, this, &JsonRule::receive_JsonRule);
}

/**
 * 添加json规则
 */
void JsonRule::on_pushButton_2_clicked()
{
    QString biaoQian = ui->lineEdit->text().trimmed(); // 标签名
    QString jsonRule = ui->lineEdit_2->text().trimmed(); // json规则

    if(biaoQian.isEmpty())
    {
        QMessageBox::information(this, "提示", "标签名不能为空");
        ui->lineEdit->setFocus();
        return;
    }

    if(jsonRule.isEmpty())
    {
        QMessageBox::information(this, "提示", "JSON表达式不能为空");
        ui->lineEdit_2->setFocus();
        return;
    }

    // 子窗口信号的发射
    emit addJsonRule(biaoQian, jsonRule); // 提交标签名字和json规则

    this->close();
}

/**
 * 接收子窗口发送来的数据
 */
void JsonRule::receive_JsonRule(QString jsonRule)
{
    ui->lineEdit_2->setText(jsonRule);
}

void JsonRule::on_pushButton_3_clicked()
{
    this->close();
}

