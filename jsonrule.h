#ifndef JSONRULE_H
#define JSONRULE_H

#include <QDialog>

namespace Ui {
class JsonRule;
}

class JsonRule : public QDialog
{
    Q_OBJECT

public:
    explicit JsonRule(QWidget *parent = nullptr);
    ~JsonRule();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void receive_JsonRule(QString jsonRule); // 接收信号

    void on_pushButton_3_clicked();

signals:
    void addJsonRule(QString biaoQian, QString jsonRule);

private:
    Ui::JsonRule *ui;
};

#endif // JSONRULE_H
